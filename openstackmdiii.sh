#!/bin/bash
#install the necessary dependencies 
sudo apt-get install pkg-config libxml2-dev libxmlsec1-dev libxmlsec1-openssl -y
sudo apt-get update 
sudo apt -y install python3-pip python3-pyscard -y
pip3 install PyOpenSSL -y
pip3 install yubikey-manager -y
sudo apt install git -y
git clone https://github.com/mehcode/python-xmlsec.git
cd python-xmlsec
pip install .
sudo apt-get install -y sshpass
ssh-keygen -q -N ""  -f open_key
openstack keypair create --public-key open_key.pub sto4_key
openstack keypair list

for machine in `echo vm1 vm2`; do

    echo "Debut creation VM $machine"

    openstack server create --network private --key-name sto4_key --flavor ds512M --image ubuntu $machine

    openstack floating ip create public

    foo=`openstack floating ip list | egrep -e "None.*\|.*None"`

    if [ ! -z "$foo" ]; then

        #$value= echo "$foo" | head -n1 | awk '{print $1;}'
        bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
        # La variable bar contient l'adresse ip flottante
        echo "Floating IP = $bar"
    else
        echo "Error"
        exit 1
    fi

    case $machine in

        vm1)
            ip_vm1=$bar; echo $ip_vm1;;

        vm2)
            ip_vm2=$bar; echo $ip_vm2;;
        vm3)
            ip_vm3=$bar; echo $ip_vm3;;
        vm4)
            ip_vm4=$bar; echo $ip_vm4;;
        vm5)
            ip_vm5=$bar; echo $ip_vm5;;
        *)
            echo "Error";;
    esac

    openstack server add floating ip $machine $bar
   echo "Je dors pendant 20s"
   sleep 38

  # controle du demarrage instance
    # a=`openstack server list --name ${machine} --status ACTIVE`
    # echo "$a++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!!"
    # while [ -z "$a" ]; do
    #   echo "Please wait"
    #   sleep 1
    #   a=`openstack server list --name ${machine} --status ACTIVE`
    # done
    echo "ubuntu@$bar"
     sshpass -f password.txt ssh-copy-id  -i ./open_key.pub root@"$bar"
    #ssh -o "StrictHostKeyChecking no" root@$bar
     ssh -o "StrictHostKeyChecking=no" -o PasswordAuthentication=no ubuntu@$bar
    #ssh -o "StrictHostKeyChecking no" -o PasswordAuthentication=no $bar
    # /usr/bin/sshpass -f ./password.txt /usr/bin/ssh-copy-id -o StrictHostKeyChecking=no -i ./open_key.pub root@"$bar"
    # sshpass -f password.txt ssh -o StrictHostKeyChecking=no root@${bar}
    #sshpass -f password.txt ssh-copy-id  -i ./open_key.pub root@"$bar"

    echo "Fin de la creation VM $machine"
done
   
    echo "Je dors pendant 45s"
    sleep 45


for machine in `echo vm1 vm2 `; do

    case $machine in

        vm1)
            #source script/installation.sh
            echo "Installation Apache2"
            ssh -i open_key ubuntu@$ip_vm1 "sudo sed -i '$ a nameserver 8.8.8.8  nameserver 8.8.4.4' /etc/resolv.conf;sudo apt update ; sudo apt install -y apache2; sudo systemctl start  apache2 ;systemctl enable apache2";;
        vm2)
           #source script https://gitlab.com/jenkinsproj/syncthing.git
            echo "Installation syncthing"
            ssh -i open_key ubuntu@$ip_vm2 "sudo sed -i '$ a nameserver 8.8.8.8  nameserver 8.8.4.4' /etc/resolv.conf;sudo apt update ; sudo apt install -y git ; git clone https://gitlab.com/jenkinsproj/syncthing.git ; cd syncthing ;sudo chmod a+x synthing.sh ; source synthing.sh";;
        vm3)
           #source script https://gitlab.com/jenkinsproj/netcloud.git
            echo "Installation NextCloud"
            ssh -i open_key ubuntu@$ip_vm3 "sudo sed -i '$ a nameserver 8.8.8.8  nameserver 8.8.4.4' /etc/resolv.conf;sudo apt update ; sudo apt install -y git ; git clone https://gitlab.com/jenkinsproj/netcloud.git ; cd netcloud ;sudo chmod a+x NextCloud.sh ; source NextCloud.sh";;   
            
        vm4) 
          #source script https://gitlab.com/jenkinsproj/ERPodoo.git   
           echo  "installation odoo"
           ssh -i open_key ubuntu@$ip_vm4 "sudo sed -i '$ a nameserver 8.8.8.8 nameserver 8.8.4.4' /etc/resolv.conf ; sudo apt update";
           sudo apt install - y git ;
           git clone https://gitlab.com/jenkinsproj/ERPodoo.git ; 
           cd ERPodoo; 
           sudo chmod a+x odoo-install.sh ;
           source odoo-install.sh;
           sudo service odoo-server restart;;


        *)

          echo "Unknown" ;;

    esac

done



